package uz.pdp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppMardon2Application {

    public static void main(String[] args) {
        SpringApplication.run(AppMardon2Application.class, args);
    }

}
